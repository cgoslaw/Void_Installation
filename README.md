# Void Installation on ZFS and Luks
**Attention!!! This setup is still not working 100%. Follow at your own risk!**

It had been some time since I tried some Linux setups so here's my first one since I've set up my this git. Although I use VoidLinux as my workstation, I've been focusing only on *BSDs lately, namely OpenBSD and FreeBSD. Still, ever since I stumbled upon Void, I wasn't use any another Linux distro. Then some weeks ago I had the idea to test some new setups with Void and this one came to mind. It's not an easy setup, especially because I'm not as seasoned as I would like, but I've been having some fun with it - as well as pulling my hair out.

I still haven't been able to set this up as I didn't have time to look into what is exactly causing the zpool to be busy when I try to export it. ***I tried checking it out with lsof and fuser, but everything seems okay. Stopped some services and killed some processes but the zpool is still busy***.
Anyhow, this is what I've done so far.

I'm thinking into switching from rEFInd to grub, and change some things as to see if I'm making some mistake but I'll leave it for when I get some free time.
Lastly, feel free to follow it at your own risk. It may even end up working with some tweaks.

## Setup
1. Download an Ubuntu iso and boot from it

2. Log into root
```
$ sudo su
```
3. Clean the disk
```
# wipefs --force --all /dev/vda
```
4. Prepare the disk
```
# parted -s -a opt /dev/vda mklabel gpt
# parted -s -a opt /dev/vda mkpart ESP fat32 1MiB 513MiB
# parted -s -a opt /dev/vda set 1 boot on
# parted -s -a opt /dev/vda mkpart rootfs 513MiB 99%
```
5. Format the ESP fs
```
# mkfs.vfat /dev/vda1
```
6. Create a LUKS container and open it
```
# cryptsetup luksFormat --hash=sha512 --key-size=512 --cipher=aes-xts-plain64 --use-urandom /dev/vda2
# cryptsetup luksOpen /dev/vda2 cryptroot
```
7. Set up the swap and the zfsroot
```
# parted -s -a opt /dev/mapper/cryptroot mklabel gpt
# parted -s -a opt /dev/mapper/cryptroot mkpart swap 0% 1024MiB
# parted -s -a opt /dev/mapper/cryptroot mkpart rootfs 1024MiB 100%
# mkswap /dev/mapper/cryptroot1 
# swapon /dev/mapper/cryptroot1
```
8. Set hostid to avoid zfs thinking the pool is being imported by the wrong system
```
# head /dev/urandom | tr -dc a-f0-9 | head -c 13 > /etc/hostid
```
9. Install zfsutils in the live CD
```
# apt-get install zfsutils-linux
# modprobe zfs
```
10. Setup the ZFS zpool
```
# zpool create -f -R /mnt -O mountpoint=none -O relatime=on -O xattr=sa -O compression=on -O normalization=formD -o ashift=12 zroot /dev/mapper/cryptroot2 
```
11. Setup the ZFS datasets
```
# zfs create -o canmount=off                        zroot/ROOT
# zfs create -o mountpoint=/                        zroot/ROOT/system
# zfs create -o mountpoint=/boot                    zroot/boot
# zfs create -o mountpoint=/home                    zroot/home
# zfs create -o canmount=off                        zroot/VAR
# zfs create -o mountpoint=/var                     zroot/VAR/var
```
12. Set the dataset to boot from
```
# zpool set bootfs=zroot/ROOT/system zroot
```
13. Fetch Void Linux tarball and extract to target dir
```
# wget https://alpha.de.repo.voidlinux.org/live/current/void-x86_64-ROOTFS-20191109.tar.xz
# tar xfv ./void-x86_64-ROOTFS-20191109.tar.xz /mnt/
```
14. Create a keyfile
```
# dd if=/dev/urandom of=/mnt/boot/rootkey.bin bs=512 count=4
# cryptsetup luksAddKey /dev/vda2 /mnt/boot/rootkey.bin
# ln -sf /dev/mapper/cryptroot2 /dev
```
15. Set up crypttab
```
# echo "cryptroot2 UUID=16030917814285301915 /boot/rootkey.bin luks" >> /mnt/etc/crypttab
```
16. Mount the pseudo-filesystems for chroot
*Mounting the pseudo-filesystems with rbind and rslave might be what is causing the zpool to be busy(?) Might check mounting with no recursion later*
```
# for i in /sys /dev /proc; do echo "mounting $i..."; mount --rbind $i /mnt$i && mount --make-rslave /mnt$i; echo "done!"; done
```
17. Copy DNS resolver config
```
cp -p /etc/resolv.conf /mnt/etc/
```
18. Copy the hostid into the actual system
```
cp /etc/hostid /mnt/etc/
```
19. Chroot into the systemfs
```
PS1='(chroot) # ' chroot /mnt /bin/bash
```
## Configuration inside the chroot
20. Configure the hostname
```
echo "void_os" > /etc/hostname
```
21. Edit /etc/rc.conf to set time settings
```
# vi /etc/rc.conf
```
22. Set the desired locales in the /etc/default/libc-locales and reconfigure the glibc package
```
# vi /etc/default/libc-locales
# xbps-reconfigure -f glibc-locales
```
23. Set the specific locale options in the /etc/locale.conf

24. Set the root passwd
```
# passwd
```
25. Install necessary packages
```
xbps-install -Suv
```
26. Install critical packages
```
xbps-install -Sv linux cryptsetup zfs refind
```
27. Disable resume from dracut
```
# echo "omit_dracutmodules+=\" resume \"" > /etc/dracut.conf.d/omit-resume-for-zvol-swap.conf
```
28. Dracut deprecated get overwritten 
```
echo -e "hostonly=yes\ninstall_items+=\" /boot/rootkey.bin /etc/crypttab \"" >> /etc/dracut.conf.d/10.crypt.conf
```
29. Reconfigure kernel package
```
# xbps-query -x linux | head -n1 | cut -f1 -d">"
# xbps-reconfigure -f linux5.9
```
30. Install rEFInd
```
# mkdir /boot/efi
# mount /dev/vda1 /boot/efi
# refind-install
```
31. Configure refind parameters in /boot/refind_linux.conf
```
# blkid -o export /dev/vda2 | grep "^UUID=" >> /boot/refind_linux.conf 
# blkid -o export /dev/mapper/cryptroot1 | grep "^UUID=" >> /boot/refind_linux.conf 

# vim /boot/refind_linux.conf
"Boot with defaults" "cryptdevice=/dev/disk/by-uuid/<uuid>:cryptroot zfs=zroot/ROOT/default rw resume=UUID=<swap UUID>"
```
32. Setting uo zfs cache
```
# zpool set cachefile=/etc/zfs/zpool.cache zroot
```
33. Configure fstab
```
# findmnt -kl -o SOURCE,UUID,TARGET,FSTYPE,OPTIONS --real >> /etc/fstab 
# echo '/dev/mapper/cryptroot1 none swap sw 0 0' >> /etc/fstab
```
34. Exit chroot, unmount the pseudo-filesystems and the ESP
```
# exit
# umount /mnt/boot/efi
# for i in /sys /dev /proc; do echo "unmounting $i..."; umount -R /mnt$i; echo "done!"; done
```
35. Unmount the zfs datasets and export the pool:
*I'm not being able to export the zfspool for some reason. I have some ideas but still haven't checked them out*
```
# zfs unmount -a
# zpool export -a -f
```
36. Reboot
